---
title: "Analytics Project Example"
author: Joshua Hyatt
title-block-banner: true
date: today
jupyter: analysis
execute:
  warning: false
---

```{python}
import pandas_profiling
import pandas as pd # [data table manipulation](https://pandas.pydata.org/docs/)
#[Parallel computing and disk-caching](https://joblib.readthedocs.io/en/latest/)
#[Functional programming helpers](https://docs.python.org/3/library/functools.html)
#[Machine learning](https://scikit-learn.org/0.21/documentation.html)
import yellowbrick as yb # [Data science plots](https://www.scikit-yb.org/en/latest/index.html)
from yellowbrick.features import rank2d
from dtreeviz.trees import * # [Decision tree explainer](https://github.com/parrt/dtreeviz)
from sklearn import datasets
from sklearn.model_selection import train_test_split, GridSearchCV, cross_val_score
from sklearn import linear_model, tree, dummy, svm
from sklearn import metrics
#[Render vertex/edge graphs](https://graphviz.readthedocs.io/en/stable/)
import matplotlib.pyplot as plt # [Generic Plotting](https://matplotlib.org/stable/contents.html#)
import seaborn as sns
import eli5 # [Interpretability](https://eli5.readthedocs.io/en/latest/)
#[Vector/Matrix mathematics](https://numpy.org/doc/stable/)
from pycaret.classification import * # [fast ml](https://pycaret.org/)
from pycaret.datasets import get_data
import tomli
from encryption import encrypt, decrypt, RandomState
from config import save_system
```

```{python}
save_system("../machine-config.json")

with open("../pyproject.toml", mode="rb") as fp:
    config = tomli.load(fp)

PROJECT_NAME = config["tool"]["poetry"]["name"]
RANDOM_STATE = RandomState(PROJECT_NAME).state
```

## Basic outline of the data analysis report

1. Introduction. Good features for the Introduction include:

    - Summary of the study and data, as well as any relevant substantive context, background, or
framing issues.
    - The "big questions" answered by your data analyses, and summaries of your conclusions about these questions.
        - Brief outline of remainder of paper.

        The above is a pretty good order to present this material in as well.

2. Body. The body can be organized in several ways. Here is a sample
    - Data
    - Methods
    - Analysis

    This format is very familiar to those who have written psych research papers. It often works
    well for a data analysis paper as well, though one problem with it is that the Methods section
    often sounds like a bit of a stretch: In a psych research paper the Methods section describes
    what you did to get your data. In a data analysis paper, you should describe any exploratory analysis that
    you performed and pre/post processing steps. Without the results as well, this can be pretty sterile sounding, so sometimes you should merge
    these “methods” pieces into the “Analysis” section.

    Other organizational formats are possible too. Whatever the format, it is useful to provide one or two
    well-chosen tables or graphs per question in the body of the report, for two reasons: First, graphical
    and tables can convey your points more efficiently than words; and second, your “skimming”
    audiences will be more likely to have their eye caught by an interesting graph or table than by running
    text. However, too much graphical tabular material will break up the flow of the text and become
    distracting; so extras should be moved to the Appendix.

3. Conclusion(s)/Discussion. The conclusion should reprise the questions and conclusions of the introduction,
perhaps augmented by some additional observations or details gleaned from the analysis
section. New questions, future work, etc., can also be raised here.

4. Appendix/Appendices. One or more appendices are the place to out details and ancillary materials.
These might include such items as
    - Technical descriptions of (unusual) statistical procedures
    - Detailed tables or computer output
    - Figures that were not central to the arguments presented in the body of the report
    - Computer code used to obtain results.

In all cases, and especially in the case of computer code, it is a good idea to add some text sentences
as comments or annotations, to make it easier for the uninitiated reader to follow what you are doing.
It is often difficult to find the right balance between what to put in the appendix and what to put in
the body of the paper. Generally you should put just enough in the body to make the point, and refer
the reader to specific sections or page numbers in the appendix for additional graphs, tables and other
details.

---

# Example Report

---

# Classifying Irises by Field Measurements

*Classification is a machine learning technique used to predict group membership for data instances.
The problem concerns the identification of IRIS plant species on the
basis of plant attribute measurements. Classification of IRIS data set would be discovering patterns
from examining petal and sepal size of the IRIS plant and how the prediction was made from analyzing the
pattern to form the class of IRIS plant. By using this pattern and classification, it is possible to
accurately predict the IRIS Species. Support Vector Machines (SVM) have been
deployed to perform classification of datasets efficiently. In this work, dimensionality
reduction techniques such as PCA will be used in conjunction with SVM to classify.*

## Introduction

Bio-informatics is a promising and novel research area in the 21st century. This field is data
driven and aims at understanding of relationships and gaining knowledge in biology. In order to
extract this knowledge encoded in biological data, advanced computational technologies,
algorithms and tools need to be used. Basic problems in bio-informatics like protein structure
prediction, multiple alignments of sequences, phylogenic inferences, etc are inherently nondeterministic polynomial-time hard in nature. To solve these kinds of problems artificial
intelligence (AI) methods offer a powerful and efficient approach. Researchers have used AI
techniques like Artificial Neural Networks (ANN), Fuzzy Logic, Genetic Algorithms, and
Support Vector Machines to solve problems in bio-informatics.

The concern of this study is towards the identification of IRIS plants on the basis of the
following measurements: sepal length, sepal width, petal length, and petal width.
This paper is mainly concerned with an analysis of the performance results of classification techniques
such as Decision Tree, Support Vector Machine (SVM) in determining the species of the Iris plants.

## Data

One of the most popular and best known databases of the neural network application is the IRIS
plant data set which is obtained from UCI Machine Learning Repository and created by R.A.
Fisher while donated by Michael Marshall (MARSHALL%PLU@io.arc.nasa.gov) on July, 1988

The IRIS dataset classifies three different classes of IRIS plant by performing pattern
classification. The IRIS data set includes three classes of 50 objects each, where each class
refers to a type of IRIS plant. The attributed that already been predicted belongs to the class of
IRIS plant. The list of attributes present in the IRIS can be described as categorical, nominal and
continuous. The experts have mentioned that there isn’t any missing value found in any attribute
of this data set. The data set is complete.

This project makes use of the well known IRIS dataset, which refers to three classes of 50
instances each, where each class refers to a type of IRIS plant. The first of the classes is linearly
distinguishable from the remaining two, with the second two not being linearly separable from
each other. The 150 instances, which are equally separated between the three classes, contain the
following four numeric attributes:

```{python}
#| tbl-cap: Iris Measures
data = get_data("iris")
decoder = ['setosa', 'versicolor', 'virginica']
```

The fifth attribute is the predictive attributes which is the class attribute that means each instance
also includes an identifying class name, each of which is one of the following: IRIS Setosa, IRIS
Versicolour, or IRIS Virginica.

[Data Exploration Results](data-exploration.html)
```{python}
report = data.profile_report(progress_bar=False)
report.to_file("../public/data-exploration.html", silent=True)
```

## Methods

Dimensionality reduction is a really important concept in Machine Learning since
it reduces the number of features in a dataset and hence reduces the
computations needed to fit the model. **PCA** is one of the well known efficient
dimensionality reduction techniques. in this tutorial we will use PCA which
compresses the data by projecting it to a new subspace that can help in reducing
the effect of the **curse of dimensionality**. Our dataset consists of
4 dimensions(4 features) so we will project it to a 2 dimensions space and
plot it for visualization.

```{python}
# Always normalize before using PCA
environment = setup(data, target="species", train_size = .8, session_id = RANDOM_STATE, pca=True, pca_components = 2, silent=True, verbose = False, normalize = True)

pca = get_config("prep_pipe")[-1].pca # the pipeline contains the fitted pca transformer
```

```{python}
#| tbl-cap: Reduced Dimension Measurements
reduced = get_config("X")
reduced.head(5)
```

The first principle component has the highest explained variance, followed by the 2nd component. By plotting these 2 components, we get a view of the 4 dimensions with as much explained variance as possible for a linear transformation. Although not a guarantee to be beneficial, often as in this case, clear separation of the target class can be observed in the transformed subspace. 

```{python}
# Visualize Space
#graph comp 1, 2 using colors from species
fig = sns.scatterplot(reduced, x="Component_1", y="Component_2", hue=data["species"]).set(title="IRIS Species Scatter Plot \n Explained Variance: %.3f" % pca.explained_variance_ratio_.sum())
```
### Feature Importance
While PCA is great for dimensionality reduction for visualization, we do lose some interpretability in how the scientific measurements are correlated to the target species. For this, we look at feature rank and feature importance charts to determine the measurements most affecting the separation between classes.

```{python}
environment = setup(data, target="species", train_size = .8, session_id = RANDOM_STATE, silent=True, verbose = False, normalize=True)
```

```{python}
#| layout-ncol: 2
#| fig-cap: 
#|   - "Pairwise Feature Correlation"
#|   - "Target Correlation"
fig = rank2d(get_config("X_train"), get_config("y_train"))
fig = yb.target.feature_correlation.feature_correlation(get_config("X_train"), get_config("y_train"), method='mutual_info-classification')
```

<br>
Standard practice is to perform train test split on the transformed data to evaluate the model performance.
We will use the convention that splits the data in 80% training/validation and 20% test set.
Because the sample size is relatively small, we will use cross validation to prevent overfitting.
We set the random state to a constant value in order to get consistent results when we rerun the code.
The advantages of this technique are the ability to limit overfitting on a relatively small dataset.

```{python}
fig = yb.target.class_balance(get_config("y_train"), get_config("y_test"), labels = decoder)
```

## Analysis

In this section, we analyze how our primary models performed,
and discuss reasons why they did not perform even
better. Specifically, we will analyze why certain species were
commonly confused with others. Confusing **versicolor** and **virginica**
is understandable based on the boundary overlap in the PCA analysis.

The results of the SVM model on unseen data are measured on the f1 score, a
geometric mean of the precision and recall scores. This measure is considered
a more realistic measure of the overall accuracy of the classification model. Of interest is the variation in performance on each cross validation run. A standard deviation in the range of 5%-10% may see performance results on test or production data far inferior than what is reported in validation. 

```{python}
#| tbl-cap: Cross Validation Results
X_test = get_config("X_test")
y_test = get_config("y_test")

# Modeling
svm_model = create_model("svm", tol=1e-3, alpha=.0012, verbose=False) # parameters can be found with tune_model
pull()
```

For comparison, a simple decision tree was trained as well. The following metrics demonstrate the varying performance on each class of the models. Standard metrics for classification are precision, recall and f1 score for each class as well as a confusion matrix. A ROC curve would also be included to demonstrate how the model performance can very even within a confusion matrix if you take into account a rank function. Such a rank function is generally only available for models as using probability based classification such as logistic regression.


```{python}
tree_model = create_model("dt", max_depth=3, verbose = False)
winning_model = compare_models(include=[svm_model, tree_model], verbose = False)
pull()
```

Most sources would indicate at this point to choose the best performing model and then run a test on the holdout dataset. It is important to make a model selection before running a holdout test as the performance results of each model may vary on the holdout set and the winning model may change. Because the purpose of the holdout set is to evaluate a model on unseen data, we cannot retroactively choose the best model for said test. 

For demonstration purposes, the performance results for both SVM and Decision Tree on the test set are shown. In the next section, we explain why we may choose a certain model even if inferior in performance.


```{python}
#| layout-ncol: 2
# Performance

# Class Prediction Errors
fig = yb.classifier.class_prediction_error(svm_model, [], [], X_test, y_test, is_fitted=True, classes = decoder)
fig = yb.classifier.class_prediction_error(tree_model, [], [], X_test, y_test, is_fitted=True, classes = decoder)

# Confusion Matrix
fig = yb.classifier.confusion_matrix(svm_model,[],[], X_test, y_test, is_fitted = True, classes = decoder)
fig = yb.classifier.confusion_matrix(tree_model,[],[], X_test, y_test, is_fitted = True, classes = decoder)

# Classification Report
fig = yb.classifier.classification_report(svm_model,[],[], X_test, y_test, is_fitted = True, classes = decoder, support=True)
fig = yb.classifier.classification_report(tree_model,[],[], X_test, y_test, is_fitted = True, classes = decoder, support=True)
```

```{python}
message = save_model(svm_model, "models/svm-model", verbose = False)
message = save_model(tree_model, "models/tree-model", verbose = False)
```

### Model Interpretability

Unfortunately SVM uses a higher dimensional space to linearly slice the sample space into class specific hyperplanes. Visualizing how this is down is difficult, but understood well geometrically. However, the decision tree can easily be visualized and explain the predictive power of the model on the test set. This example shows a mislabeled iris, but more importantly demonstrates how that incorrect result was obtained. 

```{python}
# Build a model on the final data for deployment.
environment = setup(data, target="species", train_size = .8, session_id = RANDOM_STATE, silent=True, verbose = False) # trees don't need normalization
final_tree = finalize_model(tree_model)
message = save_model(final_tree, "models/production-model", verbose = False)

X = get_config("X")
y = get_config("y")

# Visualize predictions
fig = dtreeviz(final_tree,X, y, feature_names = list(X.columns), target_name = 'species name', class_names= decoder, X = X.iloc[106], fontname="DejaVu Sans")
fig
```

```{python}
#explanation = eli5.show_prediction(final_tree, X.iloc[-1], target_names = decoder)
explanation = eli5.explain_prediction(final_tree, X.iloc[106], target_names = decoder)
print(eli5.format_as_text(explanation), f"True Class: {decoder[y[106]]}")
```

## Conclusion
We have constructed a relatively accurate classifier for species of
Iris flowers based on the field measurements. Using PCA analysis to visualize the data
there were clear segments that could be made to classify the various species.
Feeding this data into a Support Vector Machine (SVM), we were able to achieve
a relatively high F1 score with cross validation as well as the test set, thus confirming validation results.
Similarly great results were shown for the Decision Tree model as well.

Because SVM is difficult to perform in the fields, and PCA transforms the data
into features that represent eigenvectors, this method will prove difficult to
interpret as simple rules of known biological dimensions. For field use, it
will be of interest to use other
classification methods such as decision tree that is
both as accurate if not more soe than SVM and easier to use and understand in the field for biological classification.

Of further interest is the closeness of the **versicolor** and **virginica**
species with regard to the field measurements. Future studies may explore finding another measure to help differentiate them more as we see there is some similarities in their measurements. This would greatly increase the performance of any model for the purpose of classification. 


## Appendix

Entire project, including the script to generate this document is located at [https://gitlab.com/joshuacharleshyatt/analytics-template](https://gitlab.com/joshuacharleshyatt/analytics-template)
