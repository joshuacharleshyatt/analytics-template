"""Example of running Test scripts"""
from random import randint, seed
from src.encryption import RandomState

def test_random_state():
    """_summary_
    changing seed should not influence value of RandomState
    """
    seed('1234')
    state = randint(1, 2**9)
    assert state == RandomState('1234').state
    seed('3421')
    assert state == RandomState('1234').state
