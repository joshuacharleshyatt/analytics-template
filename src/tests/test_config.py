"""Example of running Test scripts"""
from src.config import get_size

def test_get_size():
    """_summary_
        1253656 => '1.20MB'
        1253656678 => '1.17GB'
    """
    assert get_size(1253656) == '1.20MB'
    assert get_size(1253656678) == '1.17GB'
